<?php
/**
 * Utilisations de pipelines par Itinéraires
 *
 * @plugin     Itinéraires
 * @copyright  2013
 * @author     Les Développements Durables
 * @licence    GNU/GPL v3
 * @package    SPIP\Itineraires\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) return;
	


/**
 * Ajout de contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function itineraires_affiche_milieu($flux) {
	$texte = "";
	$e = trouver_objet_exec($flux['args']['exec']);

	// auteurs sur les itineraires
	if ($e and !$e['edition'] and in_array($e['type'], array('itineraire'))) {
		$texte .= recuperer_fond('prive/objets/editer/liens', array(
			'table_source' => 'auteurs',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']]
		));
	}

	// auteurs sur les étapes seulement si configuré pour
	if ($e and !$e['edition'] and in_array($e['type'], array('itineraires_etape')) and lire_config('itineraires/activer_etapes_auteurs', '')) {
		$texte .= recuperer_fond('prive/objets/editer/liens', array(
			'table_source' => 'auteurs',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']]
		));
	}

	if ($texte) {
		if ($p=strpos($flux['data'],"<!--affiche_milieu-->"))
			$flux['data'] = substr_replace($flux['data'],$texte,$p,0);
		else
			$flux['data'] .= $texte;
	}

	return $flux;
}


/**
 * Ajout de liste sur la vue d'un auteur
 *
 * @pipeline affiche_auteurs_interventions
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function itineraires_affiche_auteurs_interventions($flux) {
	if ($id_auteur = intval($flux['args']['id_auteur'])) {

		$flux['data'] .= recuperer_fond('prive/objets/liste/itineraires', array(
			'id_auteur' => $id_auteur,
			'titre' => _T('itineraire:info_itineraires_auteur')
		), array('ajax' => true));

	}
	return $flux;
}

/**
 * Optimiser la base de donnees en supprimant les trucs à la poubelle 
 * et les liens orphelins
 * et les locomotions
 *
 * @param array $flux
 * @return array
 */
function itineraires_optimiser_base_disparus($flux){
	include_spip('action/editer_liens');

	// Les itinéraires à la poubelle
	$flux['data'] +=  sql_delete("spip_itineraires", "statut='poubelle' AND maj < ".sql_quote(trim($flux['args']['date'], "'")));

	// Les locomotions d'itinéraires qui n'existent plus
	$flux['data'] += sql_query(
		'delete L
		from spip_itineraires_locomotions as L
		left join spip_itineraires as I
		on L.id_itineraire=I.id_itineraire
		where I.id_itineraire is null'
	); 
	
	// Les liens
	$flux['data'] += objet_optimiser_liens(array('itineraire'=>'*'),'*');
	
	return $flux;
}

/**
 * Pipeline jqueryui_forcer pour demander au plugin l'insertion des scripts pour .sortable()
 *
 * @param array $plugins
 * @return array
 */
function itineraires_jqueryui_plugins($plugins) {
	include_spip('inc/config');
	
	// On envoie que si on est dans l'espace prive et qu'il y a des étapes
    if(test_espace_prive() and lire_config('itineraires/activer_etapes', false)) {
		$plugins[] = "jquery.ui.core";
		$plugins[] = "jquery.ui.widget";
		$plugins[] = "jquery.ui.mouse";
		$plugins[] = "jquery.ui.sortable";
		$plugins[] = "jquery.ui.droppable";
		$plugins[] = "jquery.ui.draggable";
    }
    
	return $plugins;
}

/**
 * Ajouter les autres étapes pour naviguer dans le même itinéraire
 */
function itineraires_boite_infos($flux) {
	if ($flux['args']['type'] == 'itineraires_etape') {
		$flux['data'] .= recuperer_fond('prive/squelettes/inclure/itineraires_etape_contexte', array('id_itineraires_etape'=>$flux['args']['id']));
	}
	
	return $flux;
}

/**
 * Ajouter un param en plus autorisé au modèle GIS, pour savoir si on est dans l'admin ou dans le site
 */
function itineraires_gis_modele_parametres_autorises($flux) {
	$flux[] = 'prive';
	
	return $flux;
}

/**
 * Prendre le logo d'une étape si rien trouvé dans l'itinéraire lui-même
 *
 * @param array $flux
 * @return array
 */
function itineraires_quete_logo_objet($flux) {
	// Si personne n'a trouvé de logo avant et que c'est pas pour le survol et que c'est pour un itinéraire
	if (
		empty($flux['data'])
		and $flux['args']['objet'] == 'itineraire'
		and $id_itineraire = intval($flux['args']['id_objet'])
	) {
		include_spip('base/abstract_sql');
		
		// On cherche toutes les étapes
		if ($etapes = sql_allfetsel('id_itineraires_etape', 'spip_itineraires_etapes', "id_itineraire = $id_itineraire", '', 'rang asc')) {
			$etapes = array_column($etapes, 'id_itineraires_etape');
			
			foreach ($etapes as $id_itineraires_etape) {
				if ($trouve = quete_logo_objet($id_itineraires_etape, 'itineraires_etape', $flux['args']['mode'])) {
					$flux['data'] = $trouve;
					break;
				}
			}
		}
	}
	
	return $flux;
}
