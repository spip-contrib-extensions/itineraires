# Itinéraires

Ce plugin permet la création d'itinéraires composées d'une suite d'étapes dans un ordre donné, chacune pouvant être géolocalisée.

Il peut être utilisé pour lister des randonnées, des parcours touristiques, des balades culturelles…

Il est possible de l'utiliser en configuration simplifiée uniquement avec les itinéraires seuls, ou en activant la gestion fine des étapes.