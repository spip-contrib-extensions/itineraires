<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	//C
	'configurer_titre' => 'Configurer les itinéraires',
	'configurer_activer_etapes_auteurs_label' => 'Auteurs sur les étapes',
	'configurer_activer_etapes_auteurs_label_case' => 'Activer la gestion des auteurs sur chacune des étapes',
	'configurer_activer_etapes_label' => 'Étapes',
	'configurer_activer_etapes_label_case' => 'Activer la gestion des étapes pour chaque itinéraire',
	'configurer_difficulte_max_label' => 'Difficulté maximum',
);
